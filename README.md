# Developing Benchmarking Resources for Gymnosperm Transcriptomes and Genomes
Gymnosperms, a group of plants characterized by their naked seeds, are both economically and ecologically important. Current efforts to understand gymnosperm biology have been limited by their large genome size (4 to 40Gb), high levels of heterozygosity, and large number of repetitive elements. As such, many studies focus on transcriptomes. The coding region is fairly conserved across plants, and most eukaryotes, and can provide a direct measure of how an organism is responding to biotic and abiotic challenges. In the absence of a reference genome, a de novo assembly of either long or short reads is used to create contigs that represent unique genes. Alignment of the input reads back to the assembled contigs provides an estimate of what genes are expressed over time, in a specific tissue, or in response to a specific stressor. This is facilitated by having replicated libraries for the conditions or tissues that are the focal point for the comparison. While these de novo assembled genes are not anchored to physical positions in a reference genome, they do represent the coding region of the genome. If enough libraries are sequenced (representing a diverse set of tissues over many developmental stages), the transcriptome can get close to representing the total set of genes expressed (unigene set). Since reference genomes are becoming more accessible for these large and complex genomes, benchmarks to evaluate their ‘completeness’ benefit from having a set of conserved, single-copy genes, that are available for gymnosperms, conifers, as well as genus-specific sets. 

This project will identify these sets of conserved genes through a well vetted and reproducible [workflow](https://gitlab.com/PlantGenomicsLab/gymnosperm-gene-conservation/-/tree/main/nextflow) that incorporates the benefits of many different assembly methods. Following this, I will publish my findings and release the set of conserved gymnosperm genes to the public for benchmarking of future genome and transcriptome assemblies. 

# Table of Contents
[[_TOC_]]

## 1. Pre-assembly processing
Depends where you get your reads from. I was using publically available data on the NCBI/SRA database. Here is the overall process.

- Downloading data from NCBI using sratoolkit
- Trimming and Quality Control using fastp
- Unzip data for ORP using pigz
- Renamed SRA headers to associate paired end reads using sed


<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=Pre-process
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general 
#SBATCH --qos=general
#SBATCH --mem=100G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=charles.demurjian@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load sratoolkit
module load pigz/2.2.3
module load fastp/0.23.2

fastq-dump --split-files --gzip SRR14381652
fastp -i SRR14381652_1.fastq.gz -I SRR14381652_2.fastq.gz -o trimmedSRR14381652_1.fastq.gz -O trimmedSRR14381652_2.fastq.gz -h SRR14381652.html

pigz -d trimmedSRR14381652_1.fastq.gz
pigz -dtrimmedSRR14381652_2.fastq.gz

sed -i '/^@/ s/ .*//' trimmedSRR14381652_1.fastq
sed -i '1~4s/$/\/1/' trimmedSRR14381652_1.fastq

sed -i '/^@/ s/ .*//' trimmedSRR14381652_2.fastq
sed -i '1~4s/$/\/2/' trimmedSRR14381652_2.fastq

</pre>

Now the reads should be in the right format and ready for ORP. Typically, for transcriptome assembly / unigene set creation, each species would need multiple different tissue types (usually control) and over 20M reads per library.

## 2. Running ORP
<h3> Preliminary steps:

First, you need to create and edit two files in your /home/CAM/user directory using touch.

```
touch .bash_profile
touch .bashrc
```

Once you use touch to create the file, you can use whichever text editor you would prefer. I prefer nano. Once in the .bash_profile file, paste this block of code in it.

```
PATH=$PATH:/core/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/anaconda/install/bin
PATH=$PATH:/core/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/OrthoFinder/
PATH=$PATH:/core/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/orp-transrate
PATH=$PATH:/core/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/anaconda/install/envs/orp_salmon/bin
PATH=$PATH:/core/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/anaconda/install/envs/orp_busco/bin
PATH=$PATH:/core/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/anaconda/install/envs/orp_trinity/bin
PATH=$PATH:/core/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/anaconda/install/envs/orp_transabyss/bin
```

Then, you are going to want to paste this single line of code into your .bashrc file (also located in your home directory)

```
/core/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/anaconda/install/etc/profile.d/conda.sh
```

Then open an interactive session using the srun command below.

`srun --partition=general --qos=general --pty bash`

and then run these sets of commands

```
conda activate
conda init bash
source activate orp
```

If everything worked correctly, you should have an (orp) to the left of your bash, along with a bunch of other conda init bash stuff. You then need to restart your bash before using ORP. Be sure to exit out of the interactive session and the terminal shell, and reopen it.
To run an ORP script, all you need to do is open an interactive session, and run the two commands posted below (conda active, source activate orp). To test your ORP script, sample data exists at Oyster_River_Protocol/sampledata/.

```
conda activate
source activate orp
```

and then sbatch your ORP script mirroring what it looks like below. Point towards the two reads you have and you can remove lineage if you do not want to run busco.

<h3> Script:

Below is what the script looks like for balsam library 1:

<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general 
#SBATCH --qos=general
#SBATCH --mem=150G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=charles.demurjian@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
echo "\nStart time:"
date
module load gcc/10.2.0
export TMPDIR=/core/labs/Wegrzyn/GymnoBusco/temp

/core/labs/Wegrzyn/local_software/Oyster_River_Protocol/oyster.mk CPU=24 MEM=150 \
  STRAND=RF \
  READ1=/core/labs/Wegrzyn/GymnoBusco/data/crfo/trimmedSRR12149495crfo_1.fastq \
  READ2=/core/labs/Wegrzyn/GymnoBusco/data/crfo/trimmedSRR12149495crfo_2.fastq \
  LINEAGE=viridiplantae_odb10 \
  RUNOUT=9495crfo \


</pre> 

If the run was successful, the following files will be seen in the `assemblies` directory:
    RUNOUT.filter.done
    RUNOUT.flagstat
    RUNOUT.ORP.diamond.txt
    RUNOUT.ORP.fasta
    RUNOUT.ORP.intermediate.fasta.clstr
    RUNOUT.ORP.intermediate.fasta
    RUNOUT.orthomerged.fasta
    RUNOUT.spades55.fasta
    RUNOUT.spades75.fasta
    RUNOUT.transabyss.fasta
    RUNOUT.trinity.Trinity.fasta
The most important file is the ORP fasta file:
    RUNOUT.ORP.fasta

## 3. Frame Selection with TransDecoder
Because ORP does not remove coding regions, TransDecoder was run on each ORP.fasta file. To ensure that there were no duplicate headers amongst libraries, each was given a unique identifier using sed (shown below).

    sed 's/>/>XXXX_/g' XXXX.ORP.fasta > 1.fasta
    mv 1.fasta XXXX.ORP.fasta 

With a unique ID, TransDecoder can now be run on:
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=transdecoder
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=150G
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mail-user=charles.demurjian@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "transdecoder"

module load hmmer/3.2.1
module load TransDecoder/5.3.0

TransDecoder.LongOrfs -t ../**1.fasta**
hmmscan --cpu 16 --domtblout pfam.domtblout /isg/shared/databases/Pfam/Pfam-A.hmm **1.fasta**.transdecoder_dir/longest_orfs.pep
TransDecoder.Predict -t ../**1.fasta** --no_refine_starts --retain_pfam_hits pfam.domtblout 

</pre>
Ensure the 3 bolded sections in the above transdecoder script are all exactly the same and what you want your outputed files to have the names of.

If you want to remove all ORFs of Internals or any specific ORF type, use these commands
<pre style="color: silver; background: black;">
grep -h ">" $cds > headers.txt
grep -v "type:internal" headers.txt > no_internals.txt ##type internal can change
sed -i 's/^.//' no_internals.txt
seqtk subseq $cds no_internals.txt > no_internals.cds
</pre>

## 4. Clustering at 95% with VSEARCH
The TransDecoder nucleotide (CDS) files were concatenated into a single assembly:
    
    cat *.cds > combine.cds 

And then base pairs less than 300 were removed in an interactive session with Seqtk:

    srun --partition=general --qos=general --mem=5g --pty bash
    module load seqtk
    seqtk seq -L 300 combine.cds > combine_300.cds 
    
Following concatenation and filtering, VSEARCH was run:
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=vsearch
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 40
#SBATCH --mem=150G
#SBATCH --partition=xeon
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=charles.demurjian@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "vsearch"

module load vsearch/2.4.3

vsearch --threads 8 --log LOGFile \
        --cluster_fast ../1.combine_300.cds \
        --id 0.95 \
        --centroids 1.centroids.fasta \
        --uc 1.clusters.uc 
</pre>

## 5. Contaminant Removal with EnTAP at 50/50 Coverage
Following clustering, the assembly was complete but still contained contaminants. To remove them several steps were taken:
#### 1. Create text file of all centroids.fasta headers:
    grep -h ">" centroids.fasta > centroids.txt
#### 2. Remove the “>” from list with:
    sed -i 's/^.//' centroids.txt 
#### 3. TransDecoder creates .pep files (we have been using .cds) - you need to concatenate ALL of the library peptide files in your TransDecoder directory and bring it to your working directory with centroids.txt file.
    cat *.pep > combine.pep
#### 4. Next, you need to isolate any centroids.txt headers in that peptide file
    srun --partition=general --qos=general --mem=5g --pty bash
    module load seqtk
    seqtk subseq combine.pep centroids.txt > combine_cluster.pep 
#### 5. Run EnTAP with the peptide file (change entap_config.ini if need be [i.e. taxon])

<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=entap
#SBATCH --mail-user=charles.demurjian@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 20
#SBATCH --mem=150G
#SBATCH --qos=general
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
#SBATCH --partition=general

module load anaconda/2.4.0
module load perl/5.30.1
module load diamond/0.9.25
module load interproscan/5.25-64.0
module load TransDecoder/5.3.0

/core/labs/Wegrzyn/EnTAP/EnTAP_v0.10.8/EnTAP/EnTAP --runP --ini entap_config.ini -i Combine_cluster.pep -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.200.dmnd --threads 20
</pre>

#### 6. Isolate contaminant headers in: entap_outfiles/similarity_search/DIAMOND/overall_results/best_hits_contam.faa
    grep -h ">" best_hits_contam.faa > contam.txt
    sed -i 's/^.//' contam.txt 
#### 7. Take that contam text file to your Clustering directory (where you keep centroids.fasta) and remove contaminants
    srun --partition=general --qos=general --mem=5g --pty bash
    module load seqkit
    seqkit grep -v -f contam.txt centroids.fasta > no_contam.fasta
Now you can run busco and rnaquast on the no_contam.fasta file! This is our FINAL assembly / unigene set!

## 6. OrthoFinder: Infering Orthogroups 
**1. Isolate contam free PEP files (file we used for EnTAP)**

    #Use the no_contam.fasta file we used following EnTAP and isolate headers
        grep -h “>” no_contam.fasta > no_contam.txt
    #Remove “>” from beginning
        sed -i 's/^.//' no_contam.txt 
    #Use the “combine.pep” file we made for EnTAP and isolate all the sequences that are contam free 
	    srun --partition=general --qos=general --mem=5g --pty bash
        module load seqtk
        seqtk subseq combine.pep no_contam.txt > no_contam.pep 

Repeat this with each species

**2. Move files to single directory: /core/labs/Wegrzyn/GymnoBusco/OrthoFinder**

**3. Run OrthoFinder script:**
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=orthofinder
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=
#SBATCH -o %x_%j.output
#SBATCH -e %x_%j.error


module load OrthoFinder/2.4.0
module load muscle
module load DLCpar/1.0
module load FastME
module unload diamond
module load diamond/0.9.25
module load mcl
module load anaconda/4.4.0


orthofinder -f /core/labs/Wegrzyn/GymnoBusco/OrthoFinder -S diamond -t 16
</pre>








