params.SRR = "SRR12547557"
params.fastp = "/isg/shared/modulefiles/fastp/0.23.2"
params.pigz = "/isg/shared/modulefiles/pigz/2.2.3"
params.transdecoder = "/isg/shared/modulefiles/TransDecoder/5.3.0"
params.hmmer = "/isg/shared/modulefiles/hmmer/3.2.1"
params.singularity = "/isg/shared/modulefiles/singularity/3.9.2"

log.info """\
         U N I G E N E - N F  P I P E L I N E 1/3   
         =========================================
         SRR           : ${params.SRR}
		 fastp         : ${params.fastp}
		 pigz          : ${params.pigz}
		 transdecoder  : ${params.transdecoder}
		 singularity	: ${params.singularity}
         """
         .stripIndent()
		 

Channel
    .fromSRA(params.SRR)
    .ifEmpty { error "Cannot find any reads matching: ${params.SRR}" }
    .set { read_pairs_ch }
	
process fastp {
    tag "fastp on ${pair_id}"
    module 'fastp'
	module 'pigz'

    input:
    tuple val(pair_id), path(reads) from read_pairs_ch

    output:
    tuple val(pair_id), path("trim_${pair_id}_{1,2}.fastq") into fastp_ch, fastp_ch1
	
    script:
    """
    fastp -i ${reads[0]} -I ${reads[1]} -o trim_${reads[0]} -O trim_${reads[1]} -h ${pair_id}.html &> ${pair_id}.log
	pigz -d trim_${reads[0]}
	pigz -d trim_${reads[1]}
	"""
}

process ORP {
	module '/isg/shared/modulefiles/singularity/3.9.2'

	input:
	tuple val(pair_id), path(trimmed_reads) from fastp_ch
	val SRR from params.SRR
	
	output:
	path "${SRR}.ORP.fasta" into ORP_ch
	
	script:
	"""
	source activate orp
	/home/orp/Oyster_River_Protocol/oyster.mk STRAND=RF MEM=150 CPU=24 READ1=${trimmed_reads[0]} READ2=${trimmed_reads[1]} RUNOUT=${SRR}
	"""
}

process pre_trans {
    input:
	path transcriptome from ORP_ch
	val SRR from params.SRR
	
    output:
	path transcriptome into pre_trans_ch
	
    script:
    """
	sed -i 's/>/>${SRR}_/g' ${transcriptome}
	"""
}

process trans1 {
    module '/isg/shared/modulefiles/TransDecoder/5.3.0'

    input:
	path transcriptome from pre_trans_ch

    output:
	file "${transcriptome}.transdecoder_dir" into transdecoder_ch
	 
    script:
    """
	TransDecoder.LongOrfs -t ${transcriptome}
	"""
}

process hmm {
    module '/isg/shared/modulefiles/hmmer/3.2.1'

    input:
	path transcriptome from pre_trans_ch
	file transdecoder_ch

    output:
	file "pfam.domtblout" into pfam_ch
	
    script:
	"""
	hmmscan --cpu 16 --domtblout pfam.domtblout /isg/shared/databases/Pfam/Pfam-A.hmm ${transcriptome}.transdecoder_dir/longest_orfs.pep
	"""
}

process trans2 {
	module '/isg/shared/modulefiles/TransDecoder/5.3.0'
	
	input:
	file transdecoder_ch
    path transcriptome from params.transcriptome
	file pfam_ch
	
	output:
	file "${transcriptome}.transdecoder.cds" into transdecoderCds
    file "${transcriptome}.transdecoder.pep" into transdecoderPep

	script:
	"""
	TransDecoder.Predict -t ${transcriptome} --no_refine_starts --retain_pfam_hits pfam.domtblout
	"""
}
