params.combinecds = "/core/labs/Wegrzyn/GymnoBusco/abko/abko.combine.cds"
params.combinepep = "/core/labs/Wegrzyn/GymnoBusco/postpros/abko/combine.pep"
params.config = "/core/labs/Wegrzyn/GymnoBusco/postpros/abko/EnTAP/entap_config.ini"
params.entapDB1 = "/isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd"
params.entapDB2 = "/isg/shared/databases/Diamond/RefSeq/complete.protein.faa.200.dmnd"
params.seqtk = "/isg/shared/modulefiles/seqtk/1.3"
params.vsearch = "/isg/shared/modulefiles/vsearch/2.4.3"
params.EnTAP = "/core/labs/Wegrzyn/EnTAP/EnTAP_v0.10.8/EnTAP/EnTAP"
params.seqkit = "/isg/shared/modulefiles/seqkit/2.2.0"
params.anacoda = "/isg/shared/modulefiles/anaconda/2.4.0"
params.perl = "/isg/shared/modulefiles/perl/5.30.1"
params.diamond = "/isg/shared/modulefiles/diamond/0.9.25"
params.interproscan = "/isg/shared/modulefiles/interproscan/5.25-64.0"
params.TransDecoder = "/isg/shared/modulefiles/TransDecoder/5.3.0"


log.info """\
         U N I G E N E - N F   P I P E L I N E 2/3    
         =========================================
		 combinecds		: ${params.combinecds}
		 combinepep		: ${params.combinepep}
		 config			: ${params.config}
		 entapDB1		: ${params.entapDB1}
		 entapDB2		: ${params.entapDB2}
		 seqtk			: ${params.seqtk}
		 vsearch		: ${params.vsearch}
		 EnTAP			: ${params.EnTAP}
		 seqkit			: ${params.seqkit}
		 anaconda		: ${params.anacoda}
		 perl			: ${params.perl}
		 diamond		: ${params.diamond}
		 transdecoder	: ${params.transdecoder}
		 interproscan	: ${params.interproscan}
         """
         .stripIndent()
		 

process pre_cluster {
    module '/isg/shared/modulefiles/seqtk/1.3'

    input:
	path combinecds from params.combinecds

    output:
	path "300.${combinecds}" into cluster_ch
	
    script:
    """
	seqtk seq -L 300 ${combinecds} > 300.${combinecds}
	"""
}

process cluster {
    module '/isg/shared/modulefiles/vsearch/2.4.3'
	label 'cluster'
	
    input:
	file combinecds from cluster_ch
	
    output:
	file "centroids.${combinecds}" into pretap_ch
	
    script:
    """
	vsearch --threads 8 --log LOGFile --cluster_fast ${combinecds} --id 0.95 --centroids centroids.${combinecds} --uc uc.${combinecds} 
	"""
}

process pre_EnTAP {
    module '/isg/shared/modulefiles/seqtk/1.3'

    input:
	path centroids from params.centroids
	path combinepep from params.combinepep
	
    output:
	file "combine_cluster.pep" into entap_ch

    script:
    """
	grep -h ">" ${centroids} > centroids.txt
	sed -i 's/^.//' centroids.txt 
	seqtk subseq ${combinepep} centroids.txt > combine_cluster.pep
	"""
}

process EnTAP {
	module '/isg/shared/modulefiles/anaconda/2.4.0'
	module '/isg/shared/modulefiles/perl/5.30.1'
	module '/isg/shared/modulefiles/diamond/0.9.25'
	module '/isg/shared/modulefiles/interproscan/5.25-64.0'
	module '/isg/shared/modulefiles/TransDecoder/5.3.0'
	module 'EnTAP'
	
    input:
	file combinepep from entap_ch
	path config from params.config
	path entapDB1 from params.entapDB1
	path entapDB2 from params.entapDB2

    output:
	file "best_hits_contam.faa" into posttap_ch
	
    script:
    """
	EnTAP --runP --ini ${config} -i ${combinepep} -d ${entapDB1} -d ${entapDB2} --threads 20
	"""
}

process post_EnTAP {
    module '/isg/shared/modulefiles/seqkit/2.2.0'

    input:
	file besthits from posttap_ch
	file centroids from params.centroids
	
    output:
	file "no_contam.fasta"
	
    script:
    """
	grep -h ">" ${besthits} > contam.txt
	sed -i 's/^.//' contam.txt 
	seqkit grep -v -f contam.txt ${centroids} > no_contam.fasta
	"""
}
