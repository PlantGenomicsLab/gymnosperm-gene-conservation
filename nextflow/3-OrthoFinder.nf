params.OrthoFinder = "/isg/shared/modulefiles/OrthoFinder/2.4.0"
params.muscle = "/isg/shared/modulefiles/muscle/5.0.1428"
params.DLCpar = "/isg/shared/modulefiles/DLCpar/1.0"
params.FastME = "/isg/shared/modulefiles/FastME/2.1.5"
params.diamond = "/isg/shared/modulefiles/diamond/0.9.25"
params.mcl = "/isg/shared/modulefiles/mcl/14-137"
params.anaconda = "/isg/shared/modulefiles/anaconda/4.4.0"
pepfiles = Channel.fromPath("/path/to/pepfolder/*.pep")

log.info """\
         U N I G E N E - N F  P I P E L I N E 3/3   
         =========================================
		 OrthoFinder		: ${params.OrthoFinder}
		 muscle			: ${params.muscle}
		 DLCpar			: ${params.DLCpar}
		 FastME			: ${params.FastME}
		 diamond			: ${params.diamond}
		 mcl			: ${params.mcl}
		 anaconda			: ${params.anaconda}
         """
         .stripIndent()
		 

process Ortho {
	module 'OrthoFinder'
	module 'muscle'
	module 'DLCpar'
	module 'FastME'
	module 'mcl'
	module 'diamond'
	module 'anaconda'

	input:
	path pep from pepfiles
	
	output:	
	path "Single_Copy_Orthologue_Sequences"
	
	script:
	"""
	orthofinder -f ${inputfolder} -S diamond -t 16
	"""
}
