# Unigene Nextflow Pipeline using ORP

[[_TOC_]]

The current digital ecosystem encompassing academic data publication prevents us from achieving the maximum gain from ongoing research investments. One of this project's main goals is to ensure that this work is reproducible and of maximum benefit to the scientific community. To achieve this, my overall process is documented on git, and reproducible through Nextflow workflows that represent my genomic pipeline. 

The Nextflow workflow encompassing Unigene set generation and conserved gene identification is split up into 3 sub workflows.

1. Assembly of raw RNAseq reads through the Oyster River Protocol for a single library
    - ORP.nf: consists of data download, trimming with fastp, assembly via ORP, and frame selection with transdecoder
2. Generation of a Unigene set
    - postORP.nf: consists of clustering via VSearch and contaminat removal and gene annotation via EnTAP
3. Identification of conserved genes using multiple Unigene sets
    - OrthoFinder.nf: consists of OrthoFinder

Below is more detailed information of what inputs/outputs are needed, and how to run these specific workflows. <br>
**For each workflow, you will need to update the input parameters to direct to where your modules are stored on your cluster.**

# ORP.nf
Inputs:

- An SRA number of paired end RNA data
- Module information for fastp, pigz, transdecoder, hmmer and singularity

Outputs:

- A .cds and .pep file for that SRA dataset

The Workflow:

- Data download from SRA
- Trimming and Quality control with fastp (process fastp)
- Unzipping of data with pigz (process fastp)
- Assembly via ORP (process ORP)
- Appending a specific code to the ORP assembly (process pre_trans)
- Transdecoder best reading frame (process trans1)
- hmmer (process hmmer)
- Transdecoder predict (process trans2)

Following process trans2, your SRA dataset will now have a .cds and a .pep file, which are needed for postORP.nf

<h5>How to run ORP in the nextflow script</h5>

- You'll need to create the singularity ORP image for ORP to work. Luckily, singularity can pull from docker and the developers of ORP have a docker image already created. You'll need to run singularity pull to create a .sif image to then reference in your nextflow.config file. Before running singularity pull, if running on a cluster, you will need to add the export line below to your .bashrc. 
- Note that ORP.sif is just what I decided to name the image, and you can change that depending on what you want to do.
- Once the pull is successful (took around 2 hours on our cluster), point to that .sif file in the nextflow.config file, and as mentioned previously, be sure to update all the modulefiles information, including singularity

```
export SINGULARITY_TMPDIR=/home/some/random/temp/directory/TMP_DIR
singularity pull ORP.sif docker://macmaneslab/orp:2.3.3
```


# postORP.nf
Inputs:

- A concatenated .cds and .pep file of your species __(concatenate each libraries .cds and .pep before running this workflow)__
- An entap_config.ini file with updated taxonomy and contaminant information
- Location of your diamond DB's needed to be used for EnTAP
- Module information for seqtk, vsearch, EnTAP, seqkit, anacoda, perl, diamond, interproscan, and transdecoder

Outputs:

- The final transcriptome assembly / unigene set no_contam.fasta
- The final transcriptome peptide file (following removal of contaminants) no_contam.pep

The Workflow:

- Prior to the start of the workflow, be sure to concatenate your libraries .cds and .pep files together using cat
- Removal of sequences less than 300 base pairs using seqtk (process pre_cluster)
- Clustering via VSearch at 95 percent (process cluster)
- Creation of clustered peptide file needed to be used for EnTAP (process pre_EnTAP)
- Gene Annotation and contaminant removal via EnTAP (process EnTAP)
- Removal of contaminants via seqkit and seqtk (process post_EnTAP), thereby generation of your final assembly

Following post_EnTAP, you will have a no_contam.fasta which is your final transcriptome assembly / Unigene set!

<h5>Some addition notes for postORP.nf</h5>

- the entap_config.ini file determines how EnTAP runs. Specifically, download the sample.ini file uploaded in this folder, and change the taxon to the genus of study, along with what you want to classify as contaminants, and the overall percentages as well (we used 50/50). Note that the two fields that you must enter in (taxon= and contam=) are blank in the sample entap_config.ini file
- For more information on how to run EnTAP, visit the documentation [here:](https://entap.readthedocs.io/en/latest/Getting_Started/introduction.html)


# OrthoFinder.nf
Inputs:

- Multiple no_contam.pep files for each species of test

Outputs:

- OrthoFinder directory of results

